module.exports = {
	semi: true,
	trailingComma: 'none',
	singleQuote: true,
	printWidth: 80,
	tabWidth: 4,
	endOfLine: 'auto',
	jsxSingleQuote: true,
	bracketSpacing: false,
	bracketSameLine: false,
	singleAttributePerLine: true,
	useTabs: true,
};

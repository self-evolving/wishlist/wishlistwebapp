import * as AuthService from '../../../src/Services/AuthService';

export const mockedLogIn = jest.fn();
export const mockedLogout = jest.fn();
export const mockedSignUp = jest.fn();
export const mockedRequestResetPassword = jest.fn();
export const mockedResetPassword = jest.fn();
export const mockedChangePassword = jest.fn();
export const mockedGetAccessToken = jest.fn();
export const mockedRefreshToken = jest.fn();
export const mockedIsTokenValid = jest.fn();
export const mockedSaveAccessToken = jest.fn();
export const mockedGetRefreshToken = jest.fn();

jest.mock('../../../src/Services/AuthService', () => ({
	...jest.requireActual<typeof AuthService>(
		'../../../src/Services/AuthService'
	),
	logIn: mockedLogIn,
	logout: mockedLogout,
	signUp: mockedSignUp,
	requestResetPassword: mockedRequestResetPassword,
	resetPassword: mockedResetPassword,
	changePassword: mockedChangePassword,
	getAccessToken: mockedGetAccessToken,
	refreshToken: mockedRefreshToken,
	isTokenValid: mockedIsTokenValid,
	saveAccessToken: mockedSaveAccessToken,
	getRefreshToken: mockedGetRefreshToken
}));
